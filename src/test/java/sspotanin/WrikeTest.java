package sspotanin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class WrikeTest {

    private WebDriver driver;
    private WebElement webElement;

    @BeforeClass
    public void setUp() {
        // Create a new instance of the Chrome driver
        driver = new ChromeDriver();
        // You need to put included 'chromedriver.exe' into c:/windows/system32 to avoid driver errors
    }


    @Test
    public void confirmRegistration(){

        //driver settings
        //driver.manage().window().maximize(); //uncomment for non-full size
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        // 1.open url: wrike.com
       driver.get("https://www.wrike.com");

        webElement = driver.findElement(By.className("nav_login"));
        // 2.click "Login" button
        //If browser window has narrow width, navigation buttons are under 'side menu' button
        if (!webElement.isDisplayed()){
            driver.findElement(By.className("nav_pull")).click();
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.elementToBeClickable(By.className("login")));
            driver.findElement(By.className("login")).click();
        } else webElement.click();
        // 3.click "sign Up"
        driver.findElement(By.className("w3link")).click();
        // 4.fill it up email address(random generated)
        driver.findElement(By.name("email")).sendKeys(Utils.emailGenerator());
        // 5.click "Get started for free" button
        driver.findElement(By.className("icon-arrow-r")).click();
        // 6.check at the loaded page, that you have success confirmation of registration
        String text = driver.findElement(By.className("text")).getText();
        Assert.assertEquals(text, "Registration succeded");
        // 7.click resend button
        driver.findElement(By.id("resendEmail")).click();
        webElement = driver.findElement(By.xpath("//nav/ul/li[4]/a"));
        // 8.open "Pricing" link
        //If browser window has narrow width, navigation buttons are under 'side menu' button
        if (!webElement.isDisplayed()) {
            driver.findElement(By.className("nav_pull")).click();
            Utils.waiter(driver, webElement);
            //webElement.click(); //this code doesn't work =/
            driver.findElement(By.xpath("//nav/ul/li[4]/a")).click();
        } else webElement.click();


        driver.get("https://www.wrike.com/pricing"); //TEMPORARY!

        // 9.click "Get started for free" for professional plan
        driver.findElement(By.xpath("//h2/../div/a[@id='start-free-trial-professional']")).click();
        // 10.In appeared window fill it up another random genearted email
        driver.manage().timeouts().setScriptTimeout(15, TimeUnit.SECONDS);
        Utils.waiter(driver, driver.findElement(By.id("email")));
        driver.findElement(By.id("email")).sendKeys(Utils.emailGenerator());
        // 11.click "Create my Wrike account" button
        driver.findElement(By.id("start-project")).click();
        // 12.check at the loaded page, that you have success confirmation of registration
        text = driver.findElement(By.className("text")).getText();
        Assert.assertEquals(text, "Registration succeded");
        // 13.click resend button
        driver.findElement(By.id("resendEmaill")).click();

    }


    @AfterClass
    public void shutDown() {
        // Close all browser windows and clean operative memory
        Utils.afterClass(driver);
    }

}