package sspotanin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by SpacePunch on 06.11.2015.
 */
public class Utils {

    /* public static void main(String[] args) {
        for (int i = 0; i < 20; i++) {
            System.out.println(emailGenerator());
        }
    }*/

    public static String emailGenerator(){
        StringBuilder email = new StringBuilder();
        SecureRandom random = new SecureRandom();
        int length = (int)(Math.random()*100);
        String name = new BigInteger(length, random).toString(36);
        email.append(name);
        email.append("@");
        name = new BigInteger(length, random).toString(36);
        email.append(name);
        email.append(".com");
        return email.toString();
    }

    public static void afterClass(WebDriver driver) {
        if (driver.getWindowHandles().size() > 0) {
            driver.close();
        }
        else {
            driver.quit();
        }
    }

    public static void waiter(WebDriver driver, WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

}
